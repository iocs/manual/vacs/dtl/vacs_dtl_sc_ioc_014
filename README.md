# IOC for DTL-010 vacuum turbopumps

## Used modules

*   [vac_ctrl_tcp350](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_tcp350)


## Controlled devices

*   DTL-010:Vac-VEPT-01100
    *   DTL-010:Vac-VPT-01100
